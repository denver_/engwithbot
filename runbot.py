from telebot.async_telebot import AsyncTeleBot
import asyncio
import aiohttp
from pprint import pprint
from bs4 import BeautifulSoup
import requests

bot = AsyncTeleBot('5579892094:AAEs6ifa8OPNAcZIQk4myl9STHr_pEkOusk')


@bot.message_handler(commands=['help', 'start'])
async def send_welcome(message):
    await bot.send_message(message.chat.id, 'Give me one word')


@bot.message_handler(content_types=['text'])
async def get_message(message):
    # create url
    url = 'https://www.oxfordlearnersdictionaries.com/definition/english/' + message.text.lower()
    # define headers
    headers = {'User-Agent': 'Generic user agent'}
    # get page
    page = requests.get(url, headers=headers)
    # let's soup the page
    soup = BeautifulSoup(page.text, 'html.parser')
    pprint(soup)
    try:
        # get MP3 and definition
        try:
            # get MP3
            mp3link = soup.find('div', {'class': 'sound audio_play_button pron-uk icon-audio'}).get('data-src-mp3')
            await bot.send_audio(message.chat.id, audio=mp3link)
        except:
            await bot.send_message(message.chat.id, 'Pronunciation not found!')
        try:
            # get definition
            definition = soup.find('span', {'class': 'def'}).text
            await bot.send_message(message.chat.id, definition)
        except:
            await bot.send_message(message.chat.id, 'Meaning not found!')
    except:
        await bot.send_message(message.chat.id, 'Something went wrong...')

asyncio.run(bot.polling())
